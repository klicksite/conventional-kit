#!/bin/sh

if [ -z $ACTIONS ]; then
  export ACTIONS="generate_changelog"
fi

validate_commit() {
  confrom=$(conform enforce | grep FAIL)

  if [ ! -z $confrom ]; then
    echo "Conform error:"
    conform enforce
    exit 1
  fi
}

generate_tag() {
  current_tag=$(git describe --tags $(git rev-list --tags --max-count=1))
  
  echo "Current Release: " $current_tag
  
  git tag tmp-rc
  
  changlog=$(git-chglog tmp-rc)
  
  if $(echo $changlog| grep -q '### Fix'); then
    semver up release
  fi
  if $(echo $changlog| grep -q '### Feat'); then
    semver up minor
  fi
  if $(echo $changlog| grep -q '### BREAKING'); then
    semver up major
  fi
  
  git tag -d tmp-rc
  
  rc_release=$(semver get release  | sed -e s/^v// )
  
  echo "Next Release": $rc_release

  git tag $rc_release

  git push origin --tags
}

generate_changelog() {
  ref_tag=$(git describe --tags $(git rev-list --tags --max-count=1))
  
  echo "Reference tag: " $ref_tag
  
  git-chglog $(git describe --tags $(git rev-list --tags --max-count=1)) -o CHANGELOG.md

  if [ -z $BRANCH ]; then
    export BRANCH="master"
  fi

  git add CHANGELOG.md
  git commit -m '[skip ci] Change log for version $ref_tag [ci skip]'
  git push
}

OIFS=$IFS
IFS=','
actions=$ACTIONS
for action in $actions
do
  case $action in
    validate_commit) validate_commit ;;
    generate_tag) generate_tag ;;
    generate_changelog) generate_changelog ;;
    *) echo "Opcao Invalida!" ;;
    esac
    echo ""
done
IFS=$OIFS