# Conventional Kit

Este repositório contém uma coleção de pipes para o bitbucket-pipelines criada para auxiliar no processo de implantação de nossas apps.

## Ações disponiveis

**validate_commit:** Valida se a mensagem de commit é compativel com o conventional commit

**generate_tag:** Cria e publica uma nova tag com base nos commits

**generate_changelog:** Cria e publica o changelog com base na ultima tag

## Utilizando o pipe

Para utilizar o pipe adicione o seguinte trecho de codigo a seus steps

```bash
- pipe: docker://klickpages/conventional-kit:latest
  variables:
    ACTIONS: validate_commit,generate_tag // Set actions
```
