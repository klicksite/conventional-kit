FROM golang:alpine

RUN apk add --no-cache git curl

RUN go get github.com/talos-systems/conform
RUN go get -u github.com/git-chglog/git-chglog/cmd/git-chglog
RUN go get -u github.com/maykonlf/semver-cli/cmd/semver

COPY pipe.sh /

ENTRYPOINT ["/pipe.sh"]
